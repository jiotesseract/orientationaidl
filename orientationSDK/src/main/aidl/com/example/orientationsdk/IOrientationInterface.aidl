// IOrientationInterface.aidl
package com.example.orientationsdk;

// Declare any non-default types here with import statements

interface IOrientationInterface {
   String orientation();
}