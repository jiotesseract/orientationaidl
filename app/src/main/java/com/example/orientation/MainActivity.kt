package com.example.orientation

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.hardware.SensorManager
import android.os.Bundle
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.orientationsdk.IOrientationInterface
import com.example.orientationsdk.OrientationService
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var orientationInterface: IOrientationInterface? = null
    private var orientationServiceConnection: ServiceConnection? = null
    private var orientationServiceIntent: Intent? = null
    private val Q = FloatArray(4)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addSensorDataObserver()
        bindOrientationService()
    }

    private fun addSensorDataObserver() {
        OrientationService.sensorData.observe(this, Observer {
            SensorManager.getQuaternionFromVector(Q, it)
            orientationText.text = """
                w = ${Q[0]}
                x = ${Q[1]}
                y = ${Q[2]}
                z = ${Q[3]}
                """.trimIndent()
        })
    }

    private fun initializeSensorManager() {
        orientationInterface?.orientation()?.let {
            orientationText.text = it
        }
    }

    private fun bindOrientationService() {
        orientationServiceIntent = Intent(this, OrientationService::class.java)
        orientationServiceConnection = object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName?, binder: IBinder?) {
                orientationInterface = IOrientationInterface.Stub.asInterface(binder)
                initializeSensorManager()
            }

            override fun onServiceDisconnected(componentName: ComponentName?) {
            }
        }
        orientationServiceIntent?.let {
            orientationServiceConnection?.let {
                bindService(
                    orientationServiceIntent,
                    it,
                    Context.BIND_AUTO_CREATE
                )
            }
        }
    }
}